from django.db import models

class Item(models.Model):
    CATEGORY_CHOICES = (
        (0, "None"),
        (1, "Warframe"),
        (2, "Primary Weapon"),
        (3, "Secondary Weapon"),
        (4, "Melee Weapon"),
        (5, "Archwing"),
        (6, "Archwing Primary Weapon"),
        (7, "Archwing Melee Weapon"),
        (8, "Companion"),
        (9, "Sentinel Weapon"),
        (10, "Custom Item"),
    )

    SUB_CATEGORY_CHOICES = (
        (0, "None"),
        
        (10, "Kavat"),
        (11, "Kubrow"),
        (12, "Sentinel"),
        
        (20, "K-Drive Board"),
        (21, "Kitgun Chamber"),
        (22, "Moa Head"),
        (23, "Operator Amp"),
        (24, "Zaw Tip"),
    )

    name = models.CharField(max_length=128)
    unique_name = models.CharField(max_length=128, unique=True)
    image_name = models.CharField(max_length=128)
    tradable = models.BooleanField(default=False)
    ducats = models.SmallIntegerField(default=0)
    category = models.SmallIntegerField(default=0, choices=CATEGORY_CHOICES)
    sub_category = models.SmallIntegerField(default=0, choices=SUB_CATEGORY_CHOICES)
    build_price = models.IntegerField(default=0)
    build_time = models.IntegerField(default=0)
    skip_price = models.SmallIntegerField(default=0)

    components = models.ManyToManyField('Item', related_name='+', symmetrical=False, through='ItemComponent', through_fields=('item', 'component'))

    def __str__(self):
        return "%s => %s" % (self.name, self.unique_name)
