def update_fields(object, data):
    updated = []
    for k, v in data.items():
        try:
            current = getattr(object, k)
        except AttributeError:
            pass
        else:
            if current != v:
                #print k, '->', repr(current), '!=', repr(v)
                setattr(object, k, v)
                updated.append(k)

    return updated

def maybe_int(n):
    if n is not None and n.isdigit():
        return int(n)
    return 0
