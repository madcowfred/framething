import random
import string

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


VALID_CHARS = string.ascii_letters + string.digits


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    code = models.CharField(max_length=16, default='')

    def save(self, *args, **kwargs):
        if self.code == '':
            self.generate_code()
        super().save(*args, **kwargs)

    def generate_code(self):
        self.code = ''.join(random.choice(VALID_CHARS) for _ in range(16))

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)

#@receiver(post_save, sender=User)
#def save_user_profile(sender, instance, **kwargs):
#    instance.profile.save()
