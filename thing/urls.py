from django.urls import path, re_path

from . import views

urlpatterns = [
	path('', views.index),
    re_path(r'^(?P<code>[0-9A-Za-z]{16})/$', views.user_code, name='user_code'),
    path('home_redirect/', views.home_redirect, name='home_redirect'),
    path('update_progress/', views.update_progress),
]
